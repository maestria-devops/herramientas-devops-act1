packer {
  required_plugins { # Plugin to use
    amazon = { # AWS
      version = ">= 0.0.2"
      source  = "github.com/hashicorp/amazon"
    }
  }
}

source "amazon-ebs" "debian" { # Source from AWS
  ami_name      = "mean-packer-debian-aws" # Final name of image in AWS
  region        = "us-east-1"
  instance_type = "t2.nano"
  source_ami_filter { # Source image in AWS
    filters = {
      name                = "debian-12-amd64-*"
      root-device-type    = "ebs"
      virtualization-type = "hvm"
    }
    most_recent = true
    owners      = ["136693071363"]
  }
  ssh_username = "admin" # User for run script
}

build { # Build Image
  name = "mean-packer"
  sources = [
    "source.amazon-ebs.debian"
  ]

  provisioner "file" { # Copy Scripts to install y configure mean app
    source      = "../mean"
    destination = "/home/admin/"
  }

  provisioner "shell" { # Execute scripts to install y configure mean app
    environment_vars = [

    ]
    inline = [
      "ls -l /home/admin/",
      "cd /home/admin/mean/",
      "./install.sh",
      "./configure.sh"
    ]
  }
}











































//   provisioner "shell" {
  //     inline = [
  //       "sudo adduser mi-nuevo-usuario --disabled-password --gecos ''",
  //       "echo 'mi-nuevo-usuario:mi-contraseña' | sudo chpasswd",
  //       "sudo mkdir -p /home/mi-nuevo-usuario/.ssh",
  //       "sudo cp /home/admin/.ssh/authorized_keys /home/mi-nuevo-usuario/.ssh/",
  //       "sudo chown -R mi-nuevo-usuario:mi-nuevo-usuario /home/mi-nuevo-usuario/.ssh",
  //       "echo 'mi-nuevo-usuario ALL=(ALL) NOPASSWD:ALL' | sudo tee /etc/sudoers.d/mi-nuevo-usuario"
  //     ]
  //   }