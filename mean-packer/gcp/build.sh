export GOOGLE_APPLICATION_CREDENTIALS="./packer-sa-json"

packer init .
packer build -force --var-file=variables.pkrvars.hcl gcp-debian-mean.pkr.hcl