#!/bin/bash

# Run NodeJS App
pm2 start ./app/hello.js

STARTUP_CMD=$(pm2 startup | grep 'sudo' | sed -e 's/.*\[sudo\] //')

# Execute startup pm2 command
eval $STARTUP_CMD

# Save status pm2
pm2 save

DOMAIN_NAME="mean.app.local" # Domain for MEAN Application

# Generate nginx reverse proxy for MEAN Application
sudo tee /etc/nginx/sites-available/$DOMAIN_NAME <<EOF
    server {
        listen 80;
        listen [::]:80;
        server_name $DOMAIN_NAME;
        
        client_max_body_size 100M;

        location / {
            proxy_set_header Host \$host;
            proxy_set_header X-Real-IP \$remote_addr;
            proxy_set_header X-Forwarded-For \$proxy_add_x_forwarded_for;
            proxy_set_header X-Forwarded-SSL on;
            proxy_set_header X-Forwarded-Host \$host;
            proxy_pass http://localhost:3000;
            proxy_redirect off;
        }
    }
EOF

# enabled the site
sudo rm /etc/nginx/sites-enabled/default
sudo rm /etc/nginx/sites-available/default

sudo ln -s /etc/nginx/sites-available/$DOMAIN_NAME /etc/nginx/sites-enabled/

# Restart nginx
sudo systemctl restart nginx
sudo shutdown -r now

