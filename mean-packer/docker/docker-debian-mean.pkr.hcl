packer {
  required_plugins {
    docker = {
      version = ">= 0.0.7"
      source  = "github.com/hashicorp/docker"
    }
  }
}

source "docker" "debian" {
  image  = "debian:12"
  commit = true
}

build {
  name = "mean-packer"
  sources = [
    "source.docker.debian"
  ]

  provisioner "file" {
    source      = "../mean"
    destination = "/home/"
  }

  provisioner "shell" {
    environment_vars = [

    ]
    inline = [
      "ls -l /home/",
      "cd /home/mean/",
      "apt-get update",
      "apt-get install -y sudo wget systemd systemd-sysv",
      "./install.sh",
      "./configure.sh"
    ]
  }
  
  post-processor "docker-tag" {
    repository = "mean-packer-debian"
    tag        = ["latest"]
  }
}
