provider "aws" {
  region = var.region
}



locals {
  vpc_name = format("%s-vpc", var.environment)
  vpc_cidr = "10.0.0.0/16"
  azs      = [for k, v in var.availability_zones : "${var.region}${v}"]

  tags = {
    Terraform   = "true"
    Environment = var.environment
  }
}


module "vpc" {
  source  = "terraform-aws-modules/vpc/aws"
  version = "~> 5.0"

  name = local.vpc_name
  cidr = local.vpc_cidr

  azs             = local.azs
  private_subnets = [for k, v in local.azs : cidrsubnet(local.vpc_cidr, 8, k)]
  public_subnets  = [for k, v in local.azs : cidrsubnet(local.vpc_cidr, 8, k + 4)]

  # Single NAT Gateway
  enable_nat_gateway = true
  single_nat_gateway = true
  one_nat_gateway_per_az = true

  #enable_dhcp_options      = true
  #enable_dns_hostnames     = true
  #enable_dns_support       = true
  #dhcp_options_domain_name = "${var.environment}.internal"
  #public_subnet_private_dns_hostname_type_on_launch = "resource-name"
  #private_subnet_private_dns_hostname_type_on_launch = "resource-name"
  #private_subnet_enable_resource_name_dns_a_record_on_launch = true
  #public_subnet_enable_resource_name_dns_a_record_on_launch = true
  tags = local.tags
}
