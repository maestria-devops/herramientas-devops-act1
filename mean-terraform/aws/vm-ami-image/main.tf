provider "aws" {
  region = var.region
}

locals {
  hostname = format("%s-%s", var.environment, var.machine_name)

  tags = {
    Terraform   = "true"
    Environment = var.environment
  }
}

module "key_pair" {
  source             = "terraform-aws-modules/key-pair/aws"
  key_name           = format("%s-key", local.hostname)
  create_private_key = true
}

resource "null_resource" "pem_file" {
  provisioner "local-exec" {
    command = <<-EOT
      echo '${module.key_pair.private_key_pem}' > ./${module.key_pair.key_pair_name}.pem
      chmod 600 ./${module.key_pair.key_pair_name}.pem
    EOT
  }
}


module "security_group" {
  source = "terraform-aws-modules/security-group/aws"

  name        = format("%s-sg", local.hostname)
  description = format("Security group for %s EC2 instance.", local.hostname)
  vpc_id      = var.vpc_id

  ingress_cidr_blocks = var.ingress_cidr_blocks
  ingress_rules       = var.ingress_rules
  egress_rules        = ["all-all"]

  tags = local.tags
}

data "template_file" "startup_script" {
  template = file("${path.module}/startup-script.sh")

  vars = {
    HOSTNAME             = local.hostname
    SSH_AUTHORIZED_KEYS  = var.bastion_key
    ADDITIONAL_USER_DATA = var.user_data
  }
}

module "ec2_instance" {
  source = "terraform-aws-modules/ec2-instance/aws"

  name                        = local.hostname
  key_name                    = module.key_pair.key_pair_name
  ami                         = var.machine_ami_id
  instance_type               = var.machine_type
  monitoring                  = true
  availability_zone           = var.availability_zone
  subnet_id                   = var.vpc_subnet
  vpc_security_group_ids      = [module.security_group.security_group_id, var.bastion_ssh_security_group_id]
  associate_public_ip_address = var.associate_public_ip_address
  user_data_base64            = base64encode(data.template_file.startup_script.rendered)
  user_data_replace_on_change = true
  tags                        = local.tags


}
