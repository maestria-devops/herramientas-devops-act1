#!/bin/bash
sudo hostnamectl set-hostname ${HOSTNAME}

echo '${SSH_AUTHORIZED_KEYS}' >> /home/admin/.ssh/authorized_keys

sudo apt-get update -y

${ADDITIONAL_USER_DATA}