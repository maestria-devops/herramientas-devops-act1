variable "environment" {
  description = "Environment name"
}

variable "region" {
  type        = string
  description = "The region to use"
}

variable "machine_name" {
  type = string
}

variable "machine_type" {
  type = string
  default = "t2.micro"
}

variable "machine_ami_id" {
  type = string  
}

variable "availability_zone" {
  type        = string
  description = "The zone primary to use"
}

variable "vpc_id" {
}

variable "vpc_subnet" {
}

variable "associate_public_ip_address" {
  type = bool
  default = false
}

variable "ingress_cidr_blocks" {
  type = list(string)
}

variable "ingress_rules" {
  type = list(string)
}

variable "bastion_key" {
  type = string  
}

variable "bastion_ssh_security_group_id" {
  type = string
  default = ""
}

variable "user_data" {
  default = ""
}