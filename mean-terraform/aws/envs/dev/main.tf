provider "aws" {
  region = "us-east-1"
}

locals {
  # Get image id from AWS Console when it's created
  linux_image_id     = "ami-060c3c1c5770d3be8"
}

module "network" {
  source = "../../network"

  region             = "us-east-1"
  environment        = "dev-mean-app"
  availability_zones = ["a"]
}

output "availability_zones" {
  value = module.network.vpc_azs
}

output "subnets" {
  value = module.network.vpc_private_subnets
}

module "vm_bastion" {
  source = "../../vm-bastion"

  region            = "us-east-1"
  environment       = "dev-mean"
  availability_zone = module.network.vpc_azs["a"]
  vpc_id            = module.network.vpc_id
  vpc_subnet        = module.network.vpc_public_subnets["a"]
}

module "vm_mean" {
  source = "../../vm-ami-image"

  region                        = "us-east-1"
  environment                   = "dev-mean"
  machine_name                  = "app"
  machine_type                  = "t2.nano"
  machine_ami_id                = local.linux_image_id
  availability_zone             = module.network.vpc_azs["a"]
  vpc_id                        = module.network.vpc_id
  vpc_subnet                    = module.network.vpc_public_subnets["a"]
  associate_public_ip_address   = true
  ingress_cidr_blocks           = ["0.0.0.0/0"]
  ingress_rules                 = ["http-80-tcp"]
  bastion_key                   = module.vm_bastion.key
  bastion_ssh_security_group_id = module.vm_bastion.ssh_security_group_id  
}

