variable "environment" {
  description = "Environment name"
}

variable "region" {
  type        = string
  description = "The region to use"
}

variable "machine_name" {
  type = string
  default = "bastion"
}

variable "machine_type" {
  type = string
  default = "t2.nano"
}

variable "availability_zone" {
  type        = string
  description = "The zone primary to use"
}

variable "vpc_id" {
}

variable "vpc_subnet" {
}

variable "user_data" {
  default = ""
}